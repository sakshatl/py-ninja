#everything in python is an object


#to check the type of objects we use type() method
print(type(7)) #returns int
print(type(3.5)) #returns float
print(type(8j)) #returns complex

#to assign integer value to a variable
a = 3
b = int(5) #casting integer value
c = int('6') #casting integer value to a string
d = int(7.6) #casting integer value to a float

print(c) #prints integer 6
print(d) #prints integer 7

#to assign float value to a variable
x = float('8.9')
y = float('5')

print(x) #prints float 8.9
print(y) #prints float 5.0