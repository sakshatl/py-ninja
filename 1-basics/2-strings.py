#a string in python is anything enclosed within single or double quotes

print('hello world')
print("welcome to py-ninja repo")

#by default each print statement takes a new line.
print("goodbye", end=" ")
print("world") #this will print like [goodbye world], because the end has a value of " ".
#by default end is \n which is new line.

fruit = "apple"
print(fruit) #apple
print(fruit[2]) #prints p, the letter at index 2.
print(fruit[3]) #prints l, the letter at index 3.
#index in python are 0 based.

#slicing strings
print(fruit[0:3]) #slices the string from index 0 to index 3 (not including index 3). prints app
print(fruit) #slicing does not change the original string which is still apple.

#length of string can be found out using len() method
print(len(fruit)) #prints 5

fruit2 = "mango"
#concatining strings
print(fruit + " and " + fruit2) #prints apple and mango

cheeses = "brie, cheddar, mozzarella"
print(cheeses.split(",")) #splits cheeses at comma and returs a list

print(fruit2*3) #multiplies string, prints mangomangomango
print(fruit2.upper()) #string to uppercase
print(fruit2.lower()) #string to lowercase
