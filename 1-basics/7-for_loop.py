#For loops in python

ninjas = ['kakashi', 'naruto', 'sasuke', 'madara']
for ninja in ninjas:
    print(ninja)

stars = ['****', '***', '**', '*']
for star in stars:
    print(star)


chars = ['hannah', 'zach', 'justin', 'clay']
for char in chars:
    if char == 'justin':
        print(char, '  -He is the bad guy')
    elif char == 'clay':
        print(char, '  -He is the good guy')
    else:
        print(char)
