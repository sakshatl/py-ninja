#in python a range generates a list of numbers, which we can then iterate over with for loops

for n in range(5):
    print(n)
#this prints numbers from 0 to 5(not inclusive)
print("__________________________________")
#the range function can take 3 parameters
for n in range(2, 10):
    print(n)
#prints numbers from 2 to 10(not inclusive)
print("___________________________________")
for n in range(0,20,4):
    print(n)
#prints numbers from 0 to 20(not inclusive) in steps of 4, by default steps are 1


stars = ['***', '**', '*']
for n in range(len(stars)):
    print(stars[n])
for n in range(len(stars)-1, -1, -1):
    print(stars[n])