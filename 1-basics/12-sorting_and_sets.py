chars = ['Zach', 'hannah', 'justin', 'Bryan', 'cole', 'Clay']
print(sorted(chars))
#python sorts Capital alphabets first then Regular alpabets
nums = [1, 4, 77, 28, 13, 5, 19, 44, 31, 77, 5]
print(nums)
print(sorted(nums))

#SET in python removes duplicacy and no order is preserved.
print(set(nums)) #prints a set for nums list

for num in set(nums):
    print(num, "occurs", nums.count(num), 'times')


marks = [2, 3, 6, 6, 5, 30, 40]
marks = set(marks)
marks = sorted(marks)
print(marks[1])