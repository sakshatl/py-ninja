#operators are used to perform operations on variables and values

#Arithmetic Operators
print(5 + 5)
print(5 - 5)
print(5 * 5)
print(5 / 5) #prints 1.0, the float version
print(5 // 5) #prints 1, int version
print(3 / 2) #prints 1.5
print(3 // 2) #print 1
print(8 % 5) #prints 3, the remainder of 8 / 5 = 3
print(8 % 4) #prints 0, rem of 8 /4 = 0
print(5 ** 5) #exponention, 5 to the power 5

#expression follow BODMAS.
print(5 - 5 * 5)

z = 5 #assignment operator, == eqaulsc to operator
z += 1 #same as z = z + 1
print(z)

# == eqauls to
# != not equals
# > greater than
# < less than
# >=, <= greater than and less than equal to.

#Logical Operators
x = 17
print(x > 5 and x > 10) #and, returns true if both statements are true
a = 3
print(a < 7 or a < 1) #or, return true if either of one is true
b = 5
print(not(x > 4)) #not, reverses the result

#identity operators in python
age1 = 18
age2 = 20
age3 = 20
print(age1 is age2) #returns true if both have same value
print(age1 is not age2 ) #returns true if both do not have same value
print(age2 is not age3)


