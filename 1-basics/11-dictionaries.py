#a dictionary in python is created by a sequence of elements in {} seperated by comma
#the elements are said to be key:value pairs

dogs = {
    1: 'barky',
    2: 'scooby',
    3: 'lucy'
}
print(dogs)

#to check for keys in dictionary
print(dogs.keys())
#in the form of a list
print(list(dogs.keys()))
#to update a value in dictionary
dogs[3] = 'bruno'
print(dogs)
#to check if a key exists in dict or not
print(4 in dogs) #KEY in DICT, returns FALSE
print(3 in dogs) #returns TRUE




