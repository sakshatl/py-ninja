#a list in python is just an ordered collection of data, just like arrays in javascript
greet = "welcome to py-ninja"
print(greet.split(" ")) #it splits the string wherever wherever there is a space and result is a list.
#['welcome', 'to', 'py-ninja']

#finonacci series
fib1 = [1,1,2,3,5,8,13]
print(fib1)
fib2 = [21,34,55,89]
print(fib2)
fib = fib1 + fib2 #conactning lists
print(fib)

fruits = ['mango', 'apple', 'orange', 'grapes']
fruits[2] = 'litchi' #changes index 2 to litchi, orange to litchi
print(fruits)
fruits.append('cherry') #adds item to the last of list
print(fruits)
fruits.pop() #removes item from list at specific index or last item from list if index not specified
print(fruits)
fruits.append('cherry')
del(fruits[1]) #to remove from a particular index
print(fruits)
fruits.clear() #clears the list: returns empty list
print(fruits)

characters = ['hannah', 'zach', 'justin', 'justin', 'cole']
myList = [characters, fib, [1,2,3]]
print(myList)
print(myList[2][1])
