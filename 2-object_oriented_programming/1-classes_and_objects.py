class Ninja:
    def __init__(self, name, age, belt):
#1st argument of method in class is always self
#__init__() method is used to create objects for a class, it is a constructor.
# It initializes attributes of an object
        self.name = name
        self.age = age
        self.belt = belt
#name, age, belt are attributes or instance variables.
    def display(self):
        print('hello, my name is', self.name, '. I am', self.age, 'years old. I am a', self.belt, 'belt.')
#__init__() and display() are called methods

#creating objects from the class
ninja1 = Ninja('Ryu', 17, 'red')
ninja2 = Ninja('Kakashi', 20, 'black')
ninja3 = Ninja('Sasuke', 7, 'yellow')


#calling methods on each object, display method print information of the object.
ninja1.display()
ninja2.display()
ninja3.display()

#A CLASS IS JUST A BLUEPRINT FOR AN OBJECT, A TEMPLATE FOR OBJECTS.