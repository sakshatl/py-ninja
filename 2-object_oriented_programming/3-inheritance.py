# inheritance is the capability of a class to derive the or inherit the properties from some another class.
# the class from which the properties are derived is called the parent class
# the class which inherits the properties is called the sub/child class.
# inheritance provides re-usablity of code
class Quadilateral:
    def __init__(self, a, b, c, d):
        self.a = a
        self.b = b
        self.c = c
        self.d = d
    def perimeter(self):
        perimeter = self.a+self.b+self.c+self.d
        print('perimeter = ', perimeter)

# a child class needs to mention which class is its parent class: child_class_name(parent_class_name)
class Rectangle(Quadilateral):
    def __init__(self, a, b, c = None, d = None):
        Quadilateral.__init__(self, a, b, c, d)
# you have to invoke the __init__() of the parent class to use its attributes
# if you forget to do so you will not be able to use its attributes
# this invoking is done inside the __init__() of the subclass.
        self.a = a
        self.b = b
        self.c = self.a
        self.d = self.b
    # we don't need to set the perimeter function again since it is inherited from parent class

class Square(Quadilateral):
    def __init__(self, a, b = None, c = None, d = None):
        Quadilateral.__init__(self, a, b, c, d )
        self.a = a
        self.b = self.a
        self.c = self.a
        self.d = self.a
    # again we don't need to set the perimeter function.

r1 = Rectangle(10, 20)
s1 = Square(5)

r1.perimeter()
s1.perimeter()




