class Car:
    def __init__(self, speed  = 40):
        self.speed = speed
    def get_speed(self):
        print('the max speed of the car is', self.speed)
    def set_speed(self, speed):
        if speed < 20:
            print('enter a value between 20 & 150')
        else:
            self.speed = speed

car1 = Car()
car1.set_speed(5)
car1.get_speed()