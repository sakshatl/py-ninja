class Employee:
    def __init__(self, name, salary):
        self.name = name
        self.salary = salary
    def display_salary(self):
        print(self.name, 'has a salary of', self.salary)
class SalesPerson(Employee):
    def __init__(self, name, salary, incentive):
        Employee.__init__(self, name, salary)
        self.name = name
        self.salary = salary
        self.incentive = incentive
    def display_salary(self): # overriding only works when the methods have same names (OBVIOUSLY)
        self.salary = self.salary + self.incentive # overriding the methods from the parent class
        print(self.name, 'has a salary of', self.salary)

# IF WE HAVE TO OVERRIDE, WHY DON'T WE CREATE NEW METHODS ?? its because even if can utilize half of the the prent class method then why not.

e1 = Employee('Justin', 900)
sp1 = SalesPerson('Selena', 800, 200)

e1.display_salary()
sp1.display_salary()